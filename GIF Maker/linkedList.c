#include "linkedList.h"


/*
This function frees all dynamic memory allocated in Frame
Input: frame to free
Output: none
*/
void freeFrame(Frame* frame)
{
	free((frame->path));
	free((frame->name));
	free(frame);
}
/*
This function frees all memory allocated in a list of FrameNodes
Input: list to free
Output: none
*/
void freeList(FrameNode** list)
{
	FrameNode* temp = NULL;
	FrameNode* curr = *list;
	while (curr)	// if not NULL
	{
		temp = curr;
		curr = curr->next;
		freeFrame(temp->frame);
		free(temp);
	}

	*list = NULL;
}
/*
This function creates a FrameNode
Input: none
Output: FrameNode created
*/
FrameNode* createFrame(FrameNode* list)
{
	FrameNode* node = (FrameNode*)malloc(sizeof(FrameNode));
	Frame* frame = (Frame*)malloc(sizeof(Frame));

	printf("Enter file path: ");
	frame->path = getString();
	if (!fileExists(frame->path))
	{
		free(frame->path);
		printf("File wasn't found.\n");
		free(node);
		free(frame);
		node = NULL;	// if path doesnt exist return null
	}
	else
	{
		do
		{
			printf("Enter frame duration (in ms): ");
			frame->duration = getInt();
			if (frame->duration > MAX_DURATION || frame->duration < 1)
			{
				printf("Invalid!\n");
			}
		} while (frame->duration > MAX_DURATION || frame->duration < 1);

		printf("Enter name: ");
		frame->name = getString();
		while (checkName(list, frame->name))	// if name already in list
		{
			free(frame->name);
			printf("Name already exists!\n");
			printf("Enter name: ");
			frame->name = getString();
		}

		node->frame = frame;
		node->next = NULL;
	}

	return node;
}
/*
This function, like createFrame(), creates and returns a frame, only this function does it with parameters
Input: path of picture, duration of frame, name of frame
Output: FrameNode containing the frame
*/
FrameNode* createFrameParameters(char* path, uint duration, char* name)
{
	FrameNode* node = (FrameNode*)malloc(sizeof(FrameNode));
	Frame* frame = (Frame*)malloc(sizeof(Frame));

	frame->duration = duration;
	frame->name = name;
	frame->path = path;

	node->frame = frame;
	node->next = NULL;

	return node;
}
/*
This function adds a node to end of list
Input: pointer to pointer to head of list, new node to add
Output: none
*/
void append(FrameNode** list, FrameNode* newNode)
{
	FrameNode* curr = *list;

	if (newNode)
	{
		if (!*list) // empty list!
		{
			*list = newNode;
		}
		else
		{
			while (curr->next)
			{
				curr = curr->next;
			}

			curr->next = newNode;
		}
	}
}
/*
This function check if a name is already in a list
Input: the list to check for names in, name to check
Output: True/False if name exists
*/
int checkName(FrameNode* list, char* name)
{
	FrameNode* curr = list;
	int nameExists = FALSE;

	while (curr && !nameExists)
	{
		if (strcmp(name, curr->frame->name) == 0)
		{
			nameExists = TRUE;
		}

		curr = curr->next;
	}

	return nameExists;
}
/*
This function finds the previous frame of a wanted frame.
Input: list to search frame in, name of frame, pointer to index (OPTIONAL - if the index is wanted with the frame, pass pointer to index. else pass NULL)
Output: Previous frame of wanted frame if frame was found, else NULL
*/
FrameNode* findPrevFrame(FrameNode* list, char* name, int* index)
{
	FrameNode* curr = list, *prev = NULL;
	int frameIndex = 1;

	if (list)
	{
		if (strcmp(curr->frame->name, name) == 0)	// if head is wanted frame
		{
			prev = list;
		}
		else
		{
			while (curr && strcmp(curr->frame->name, name) != 0)
			{
				prev = curr;
				curr = curr->next;
				frameIndex++;
			}
			if (curr == NULL)	// if name was found
			{
				prev = NULL;
			}
		}
	}

	if (index)	// if index not NULL
	{
		*index = frameIndex;
	}

	return prev;
}
/*
This function removes a frame from the list.
Input: name of frame to remove, pointer to pointer to head of list
Output: none
*/
void removeFrame(FrameNode** list)
{
	FrameNode* temp = NULL;
	FrameNode* prev = NULL;
	int removed = FALSE;
	char* name = NULL;
	int orgIndex = 0;

	printf("Enter name of frame to remove: ");
	name = getString();
	if (*list)
	{
		prev = findPrevFrame(*list, name, &orgIndex);	// find frame before wanted frame (no index needed)
		if (orgIndex == 1)	// if wanted frame is head
		{
			*list = prev->next;
			freeFrame(prev->frame);
			free(prev);
			removed = TRUE;
		}
		else if (prev)
		{
			temp = prev->next;
			prev->next = prev->next->next;
			freeFrame(temp->frame);
			free(temp);
			removed = TRUE;
		}
	}

	if (!removed)
	{
		printf("frame \"%s\" not found\n", name);
	}

	free(name);
}
/*
This function moves a frame to a desired position in list
Input: list to move frame in
Output: none
*/
void moveFrame(FrameNode** list)
{
	FrameNode* curr = NULL;
	FrameNode* prev = NULL;
	FrameNode* toMove = NULL;
	int found = FALSE, destIndex = 0, i = 0, outOfBounds = FALSE, orgIndex = 0;
	char* name = NULL;

	printf("Enter name of frame to move: ");
	name = getString();
	printf("Enter index to move frame to: ");
	destIndex = getInt();
	if (destIndex < 1)
	{
		outOfBounds = TRUE;
	}

	if (*list)
	{
		prev = findPrevFrame(*list, name, &orgIndex);
		if (orgIndex != destIndex)
		{
			if (orgIndex == 1)	// extract the wanted frame
			{
				*list = prev->next;
				toMove = prev;
				found = TRUE;
			}
			else if (prev)
			{
				curr = prev->next;
				prev->next = prev->next->next;
				toMove = curr;
				found = TRUE;
			}
		}
		else
		{
			found = TRUE;
		}

		if (found)
		{
			if (orgIndex != destIndex)		// if indexes are equal - no need to move
			{
				curr = *list;
				if (destIndex == 1)		// if move to start
				{
					*list = toMove;		// head is wanted frame
					(*list)->next = curr;
				}
				else
				{
					destIndex--;
					for (i = 1; i <= destIndex && !outOfBounds; i++)
					{
						prev = curr;
						curr = curr->next;
						if (!curr && i != destIndex)	// if NULL (and not on last iteration) then index is bigger than list length
						{
							outOfBounds = TRUE;
						}
					}
					if (outOfBounds)
					{
						curr = *list;
						printf("Invalid index!\n");
						if (orgIndex == 1)	// if we have a problem - place toMove back in it's place
						{
							*list = toMove;		// head is wanted frame
							(*list)->next = curr;
						}
						else
						{
							for (i = 1; i < orgIndex; i++)
							{
								prev = curr;
								curr = curr->next;
							}
							prev->next = toMove;
							toMove->next = curr;
						}
					}
					else
					{
						prev->next = toMove;	// fit frame between prev and curr
						toMove->next = curr;
					}
				}
			}
		}
		else
		{
			printf("frame \"%s\" not found\n", name);
		}

		free(name);
	}
}
/*
This function changes the play duration of a frame
Input: list to find frame in
Output: none
*/
void changeDuration(FrameNode* list)
{
	FrameNode* prev = NULL;
	char* name = NULL;
	uint duration = 0;
	int index = 0;

	printf("Enter name of frame to change duration of: ");
	name = getString();
	do
	{
		printf("Enter new duration: ");
		duration = getInt();
		if (duration > MAX_DURATION || duration < 1)
		{
			printf("Invalid!\n");
		}
	} while (duration > MAX_DURATION || duration < 1);

	prev = findPrevFrame(list, name, &index);
	if (index == 1)		// if head
	{
		prev->frame->duration = duration;
	}
	else if (prev)
	{
		prev->next->frame->duration = duration;		// frame found is one BEFORE wanted.
	}
	else
	{
		printf("frame \"%s\" not found\n", name);
	}

	free(name);
}
/*
This function changes the duration of ALL frames in a list
Input: list to change duration in
Output: none
*/
void changeAllDurations(FrameNode* list)
{
	FrameNode* curr = list;
	uint duration = 0;

	do
	{
		printf("Enter new duration for all frames: ");
		duration = getInt();
		if (duration > MAX_DURATION || duration < 1)
		{
			printf("Invalid!\n");
		}
	} while (duration > MAX_DURATION || duration < 1);

	while (curr)
	{
		curr->frame->duration = duration;
		curr = curr->next;	// advance
	}
}
/*
This function prints all frames in a list
Input: head of list
Output: none
*/
void printFrames(FrameNode* list)
{
	FrameNode* curr = list;
	if (list)
	{
		while (curr)
		{
			printf("Name: %s\n"
				"Duration: %u\n"
				"Path: %s\n\n",
				curr->frame->name, curr->frame->duration, curr->frame->path);
			curr = curr->next;
		}
	}
}
