#ifndef LINKEDLISTH
#define LINKEDLISTH

#include "inputHandle.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define FALSE 0
#define TRUE !FALSE
#define MAX_DURATION 10000

typedef unsigned int uint;
// Frame struct
typedef struct Frame
{
	char*		name;
	unsigned int	duration;
	char*		path;
} Frame;


// Link (node) struct
typedef struct FrameNode
{
	Frame* frame;
	struct FrameNode* next;
} FrameNode;

void freeFrame(Frame* frame);
void freeList(FrameNode** list);
FrameNode* createFrame(FrameNode* list);
FrameNode* createFrameParameters(char* path, uint duration, char* name);
void append(FrameNode** list, FrameNode* newNode);
int checkName(FrameNode* list, char* name);
FrameNode* findPrevFrame(FrameNode* list, char* name, int* index);
void removeFrame(FrameNode** list);
void moveFrame(FrameNode** list);
void changeDuration(FrameNode* list);
void changeAllDurations(FrameNode* list);
void printFrames(FrameNode* list);

#endif
