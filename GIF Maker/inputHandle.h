#ifndef INPUTHANDLEH
#define INPUTHANDLEH

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define NOT_FOUND -1
#define CHUNK 1024
#define FALSE 0
#define TRUE !FALSE

char* getString();
int getInt();
int fileExists(char* path);

#endif
