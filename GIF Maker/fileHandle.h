#ifndef FILEHANDLEH
#define FILEHANDLEH

#include "inputHandle.h"
#include "linkedList.h"
#include <stdio.h>
#include <stdlib.h>

#define CHUNK 1024
#define NUM_OF_PARAMS 3

void saveVideo(FrameNode* list);
FrameNode* loadVideo();

#endif
