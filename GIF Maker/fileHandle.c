#include "fileHandle.h"

/**
This function saves a video to a file
Input: video to save
Output: none
*/
void saveVideo(FrameNode* list)
{
	FILE* saveFile = NULL;
	int choice = 0;
	int cancelled = FALSE;
	FrameNode* curr = list;
	char* filePath = NULL;

	printf("Enter file path: ");
	filePath = getString();
	if (fileExists(filePath))
	{
		printf("File Already exists. Are you sure you want to override it? (y/n) ");
		choice = getchar();
		while (getchar() != '\n');
		if (choice != 'y' && choice != 'Y')		// only if entered y\Y continue. else cancel
		{
			printf("Save cancelled.\n");
			cancelled = TRUE;
		}
	}
	if (!cancelled)
	{
		saveFile = fopen(filePath, "w");
		while (curr)
		{
			fprintf(saveFile, "%s;%u;%s\n", curr->frame->path, curr->frame->duration, curr->frame->name);
			curr = curr->next;	// advance
		}
		fclose(saveFile);
		printf("Saved successfully.\n");
	}
	free(filePath);
}
/**
This function loads a video from a file
Input: none
Output: list of FrameNodes (video)
*/
FrameNode* loadVideo()
{
	FILE* saveFile = NULL;
	FrameNode* list = NULL;
	char* frameName = NULL, *framePath = NULL, *filePath = NULL, *frameDur = NULL;
	int read = 0, error = FALSE;

	printf("Enter file path: ");
	filePath = getString();

	if (!fileExists(filePath))
	{
		printf("File \"%s\" not found. Continuing to editor...\n", filePath);	// cancel load if file doesnt exist
	}
	else
	{
		saveFile = fopen(filePath, "r");

		do
		{
			frameName = (char*)malloc(CHUNK * sizeof(char));
			framePath = (char*)malloc(CHUNK * sizeof(char));
			frameDur = (char*)malloc(CHUNK * sizeof(char));
			read = fscanf(saveFile, "%[^;];%[^;];%[^\n]\n", framePath, frameDur, frameName);	// %[^ reads until the char stated
			if (read < NUM_OF_PARAMS)	// if read less than 3 parameters
			{
				if (read != EOF)	// if it's not end of file ERROR
				{
					printf("An error has occurred. Continuing to editor... (a new project will be opened)\n");
					error = TRUE;
				}
				free(framePath);	// anyway free dynamic memory
				free(frameName);
				free(frameDur);
			}
			else
			{
				if (!fileExists(framePath) || (atoi(frameDur) > MAX_DURATION || atoi(frameDur) <= 0) || checkName(list, frameName))		// check if frame is valid
				{
					printf("Error loading \"%s\": Invalid frame.\n", frameName);
					free(frameName);
					free(framePath);
					free(frameDur);
				}
				else
				{
					frameName = (char*)realloc(frameName, sizeof(char) * (strlen(frameName) + 1));	// alloc as less as possible
					framePath = (char*)realloc(framePath, sizeof(char) * (strlen(framePath) + 1));
					frameDur = (char*)realloc(frameDur, sizeof(char) * (strlen(frameDur) + 1));
					append(&list, createFrameParameters(framePath, atoi(frameDur), frameName));
					free(frameDur);		// we add the others as char*, and this one as uint, so no need for saving it allocated
				}
			}
		} while (read == NUM_OF_PARAMS && !error);	// if error or read less than needed exit

		fclose(saveFile);
	}

	if (error)
	{
		freeList(&list);
	}

	free(filePath);
	return list;
}
