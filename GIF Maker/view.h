/*********************************
* Class: MAGSHIMIM Final Project *
* Play function declaration	 	 *
**********************************/

#ifndef VIEWH
#define VIEWH

#include <opencv2\core\core_c.h>
#include <opencv2\highgui\highgui_c.h>
#include <stdio.h>
#include "LinkedList.h"

#define GIF_REPEAT 5
#define HEIGHT 500
#define WIDTH 800

void play(FrameNode *list);
void playFrame(Frame* frame, char* windowName);
void playBackwards(FrameNode* list, char* windowName);

#endif
