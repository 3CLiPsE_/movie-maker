#include "inputHandle.h"


/*
This function gets string as user input, without '\n'
Input: none
Output: pointer to string
*/
char* getString()
{
	char* string = malloc(sizeof(char) * CHUNK);

	fgets(string, CHUNK, stdin);
	if (strcspn(string, "\n") != NOT_FOUND)	// if there's '\n'
	{
		string[strcspn(string, "\n")] = 0;	// delete it
	}
	string = (char*)realloc(string, sizeof(char) * (strlen(string) + 1));	// realloc to string + '\0'

	return string;
}
/*
This function gets int input and clears the buffer
Input: none
Output: int input
*/
int getInt()
{
	int input = 0;

	scanf("%d", &input);
	while (getchar() != '\n');

	return input;
}
/*
This function checks if a file exists.
Input: path of file
Output: true/false if file exists
*/
int fileExists(char* path)
{
	FILE* file = fopen(path, "r");
	int doesExist = FALSE;

	if (file)
	{
		fclose(file);
		doesExist = TRUE;
	}

	return doesExist;
}

