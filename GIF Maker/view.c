/*********************************
* Class: MAGSHIMIM Final Project *
* Play function				 	 *
**********************************/

#include "view.h"


/**
play the movie!!
display the images each for the duration of the frame one by one and close the window
input: a linked list of frames to display
output: none
*/
void play(FrameNode* list)
{
	cvNamedWindow("Display window", CV_WINDOW_NORMAL); //create a window
	cvResizeWindow("Display window", WIDTH, HEIGHT);
	FrameNode* head = list;
	int imgNum = 1, playCount = 0;
	IplImage* image;
	while (playCount < GIF_REPEAT)
	{
		while (list != 0)
		{
			playFrame(list->frame, "Display window");
			list = list->next;
		}
		list = head; // rewind
		playCount++;
	}
	cvDestroyWindow("Display window");

	return;
}
/**
This function plays a frame given
Input: frame to play, name of opencv window
Output: none
*/
void playFrame(Frame* frame, char* windowName)
{
	IplImage* img = cvLoadImage(frame->path, 1);	// create image

	if (!img)
	{
		printf("Could not load \"%s\".\n", frame->name);
	}
	else
	{
		cvShowImage(windowName, img);

		cvWaitKey(frame->duration);
		cvReleaseImage(&img);	// release dynamic memory
	}
}
/**
This function plays a list backawrds (recoursively)
Input: list to play, name of opencv window
Output: none
*/
void playBackwards(FrameNode* list, char* windowName)
{
	if (list)
	{
		playBackwards(list->next, windowName);	// get to last frame
		playFrame(list->frame, windowName);
	}
}
