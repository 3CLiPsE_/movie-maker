/*********************************
* Class: MAGSHIMIM C2			 *
**********************************/

#include <stdio.h>
#include <stdlib.h>
#include "linkedList.h"
#include "view.h"
#include "fileHandle.h"


#define FALSE 0
#define TRUE !FALSE

int main()
{
	int choice = 0;
	FrameNode* list = NULL;
	int i = 0;

	printf("Welcome to my GIF Editor!\n"
		"Please enter choice:\n"
		"1. Load video\n"
		"2. Continue to editor (new video)\n");
	choice = getchar();
	while (choice != '\n' && getchar() != '\n');
	switch (choice)
	{
	case '1':
		list = loadVideo();
		break;
	case '2':
		break;	// simply continue
	default:
		printf("Invalid choice! Continuing to editor...\n");
		break;
	}

	do
	{
		printf("Enter choice:\n"
			"0. Exit\n"
			"1. Add frame\n"
			"2. Remove frame\n"
			"3. Change frame location\n"
			"4. Change frame's duration\n"
			"5. Change ALL frames' duration\n"
			"6. Print frames\n"
			"7. Play video\n"
			"8. Save video\n");
		choice = getchar();
		while (choice != '\n' && getchar() != '\n');
		switch (choice)
		{
		case '0':
			printf("Bye!\n");
			break;
		case '1':
			append(&list, createFrame(list));	// add to list
			break;
		case '2':
			removeFrame(&list);
			break;
		case '3':
			moveFrame(&list);
			break;
		case '4':
			changeDuration(list);
			break;
		case '5':
			changeAllDurations(list);
			break;
		case '6':
			printFrames(list);
			break;
		case '7':
			printf("Enter choice:\n"
				"1. Play forwards\n"
				"2. Play backwards\n");
			choice = getchar();
			while (choice != '\n' && getchar() != '\n');
			switch (choice)
			{
			case '1':
				play(list);		// play list normally
				break;
			case '2':	// play list backwards
				cvNamedWindow("Display window", CV_WINDOW_NORMAL);
				cvResizeWindow("Display window", WIDTH, HEIGHT);
				for (i = 0; i < GIF_REPEAT; i++)	// play backwards times GIF_REPEAT
				{
					playBackwards(list, "Display window");
				}
				cvDestroyAllWindows();
				break;
			default:
				printf("Invalid choice. Returning to menu...\n");
			}
			break;
		case '8':
			saveVideo(list);
			break;
		default:
			printf("Invalid choice!\n");
			break;
		}
	} while (choice != '0');

	freeList(&list);
	getchar();
	return 0;
}
